# melos

### Status
Experimental; expect breaking changes.

### Dependencies
- [Supercollider](https://supercollider.github.io/)
- [Lilypond](http://lilypond.org/)
- [Stack](https://docs.haskellstack.org/en/stable/README/)

### Description
Melos is a library for music composition. It aims to support less conventional types of music -- arbitrary microtonal scales; arbitrary mappings between sounds and their representations.

Scores are written as haskell source files.

Melos targets Lilypond (for pdf scores) and SuperCollider non-realtime synthesis (for rendering the score as an audio file).

A "score" is a haskell data structure. The same haskell score generates
1. an `.ly` file, which is rendered by Lilypond into musical notation
2. an `.osc` file, which is rendered as [SuperCollider NRT](http://doc.sccode.org/Guides/Non-Realtime-Synthesis.html)

The current version of this library implements the core logic and not much else; convenience functions and safe wrappers are absent. It is therefore rather tedious and sometimes error-prone to manually build scores.

### Example

See [Example](src/Melos/Example/Simple/Main.hs) for context.

```haskell
score = do
  replicateM_ 2 $ do
    forM_ [half & dot, half] $ \measureDuration -> do
      t measureDuration $ do
        t half $ do
          l quarter $
            emptyParams & voice1 ?~ Pitch (Pitch12TET E 5)
          l quarter $
            emptyParams & voice2 ?~ Pitch (Pitch12TET D 5)
        l quarter $ emptyParams
          & voice1 ?~ Rest
          & voice3 ?~ Pitch (Pitch12TET C 5)

main = do
  (voices, spans) <- runScore score
  -- Render score as pdf via lilypond
  renderLilypond voices
  -- Render a soundfile using non-real-time synthesis in SuperCollider
  renderPiano spans
```

##### Example Output:

![Lilypond Output](img/lily-out.png)

##### A slightly more interesting example

![Lilypond Output](img/lilypond2.png)

- [Full pdf](doc/example.pdf)
- [Audio](doc/audio/example.mp3)
