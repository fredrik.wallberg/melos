{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts #-}

module Melos.Score.Tree where

import Control.Monad.State
import Data.Function
import Melos.Score.Duration (Duration (..), NoteValue (..))
import Melos.Score.Span
import Melos.Score.Stream

data Tree a b = Tree a [Tree a b] | Leaf b
  deriving (Functor, Traversable, Foldable, Show)

data Node a = Node (Duration NoteValue) a
  deriving (Show)

distribute ::
  Traversable f =>
  f (Tree a (Node g)) ->
  Stream (g -> b) ->
  f (Tree a (Node b))
distribute score params = evalState (mapM go score) params
  where
    go (Tree duration xs) = do
      xs' <- mapM go xs
      return $ Tree duration xs'
    go l@(Leaf (Node duration params)) = do
      curr <- gets takeFromStream
      modify dropFromStream
      return $ Leaf (Node duration (curr params))

-- TODO: move to lib
insertSpan (Tree ((start, end), prolation, dur) xs) = do
  spans <- get
  let spansInT = takeWhile (\(Span start' _ _) -> start' < end) spans
      spansOutsideT = dropWhile (\(Span _ end' _) -> end' == end) spans
  case Nothing of
    Nothing -> do
      xs' <- mapM insertSpan xs
      return $ Tree (Span start end (prolation, dur)) xs'
    Just durs -> error "Move duration splitting to lilypond domain"
insertSpan (Leaf (start, end, dur)) = do
  spans <- get
  let spansInT = takeWhile (\(Span start' _ _) -> start' < end) spans
      spansOutsideT = dropWhile (\(Span _ end' _) -> end' == end) spans
  put spansOutsideT
  return $ Leaf (Span start end (dur, spansInT & head & spanValue))
