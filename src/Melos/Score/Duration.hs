{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleInstances #-}

module Melos.Score.Duration where

import qualified Data.Foldable as Foldable
import Data.Function ((&))
import qualified Data.Map as Map
import qualified Data.Monoid as Monoid
import Data.Ratio (Ratio (..), denominator, numerator, (%))
import qualified Text.Pretty.Simple as PP

{- |
- create any valid duration
- syntax for durations?
- split unwritable durations
- format as lily duration

Problem -- measure base duration is not writable as one value.

Split duration as early as possible -- explicit splits with defaults?
Track nonreduced ratio to simplify printing and allow customization?

Limit numerator and denominator to "writable" durations

Template Haskell:
- generate only valid durations via Lilypond model
- generate name via Ratio's numerator / denominator (e.g. 3/8)
- Provide aliases for e.g. 5/8 via composite duration
- question: quick way to calculate actual ratio? store with value?

Nested composite durations are useful since "building blocks"" can be reused
-}
class MusicalDuration a where
  fromRatio :: Ratio Int -> Maybe a
  toRatio :: a -> Ratio Int

data TimeSignature = TimeSignature
  { _numerator :: Int
  , _denominator :: Int
  }
  deriving (Eq, Show)

data Duration a
  = SimpleDuration a
  | CompositeDuration [Duration a]
  | NonReducedDuration (Int, a)
  deriving (Eq, Show, Functor, Foldable)

data NoteValue
  = Dotted NoteValue
  | Whole
  | Half
  | Quarter
  | Eighth
  | Sixteenth
  | ThirtySecond
  | SixtyFourth
  deriving (Eq, Show)

whole = SimpleDuration Whole

half = SimpleDuration Half

quarter = SimpleDuration Quarter

eighth = SimpleDuration Eighth

sixteenth = SimpleDuration Sixteenth

thirtySecond = SimpleDuration ThirtySecond

sixtyFourth = SimpleDuration SixtyFourth

dot :: Duration NoteValue -> Duration NoteValue
dot x = fmap Dotted x

durationMap :: Map.Map (Ratio Int) NoteValue
durationMap =
  Map.fromList
    [ (1, Whole)
    , (1 % 2, Half)
    , (1 % 4, Quarter)
    , (1 % 8, Eighth)
    , (1 % 16, Sixteenth)
    , (1 % 32, ThirtySecond)
    , (1 % 64, SixtyFourth)
    ]

noteValues (SimpleDuration x) = [x]
noteValues (CompositeDuration []) = []
noteValues (CompositeDuration (x : xs)) = noteValues x <> concatMap noteValues xs

instance MusicalDuration NoteValue where
  toRatio Whole = 1 % 1
  toRatio Half = 1 % 2
  toRatio Quarter = 1 % 4
  toRatio Eighth = 1 % 8
  toRatio Sixteenth = 1 % 16
  toRatio ThirtySecond = 1 % 32
  toRatio SixtyFourth = 1 % 64
  toRatio (Dotted dur) = go [1 % 2] dur
    where
      go xs@(x : _) (Dotted dur) = go ((1 % 2) * x : xs) dur
      go xs dur = ratio + dotsDur
        where
          ratio = toRatio dur
          dotsDur = sum . fmap ((*) ratio) $ xs
  fromRatio = undefined

instance MusicalDuration (Duration NoteValue) where
  toRatio dur@(CompositeDuration xs) =
    -- Monoid.getSum . Foldable.foldMap (Monoid.Sum . toRatio) $ dur
    fmap toRatio xs & sum
  toRatio (SimpleDuration dur) = toRatio dur
  toRatio (NonReducedDuration (times, dur)) = (times % 1) * toRatio dur
  fromRatio r = case Map.lookupLE r durationMap of
    Nothing -> Nothing
    Just v -> go v
    where
      go (duration, noteValue)
        | r == duration = Just $ SimpleDuration noteValue
        | r == duration + (duration * (1 % 2)) =
          SimpleDuration noteValue
            & dot
            & Just
        | r == duration + (duration * (1 % 2)) + (duration * (1 % 4)) =
          SimpleDuration noteValue
            & dot
            & dot
            & Just
        | otherwise = Nothing

instance MusicalDuration TimeSignature where
  toRatio (TimeSignature n d) = n % d
  fromRatio r = Just $ TimeSignature (numerator r) (denominator r)

addDurations :: Ratio Int -> Ratio Int -> Maybe (Duration NoteValue)
addDurations a b = fromRatio (a + b)

main :: IO ()
main = do
  PP.pPrint quarter
  PP.pPrint $ quarter & toRatio
  PP.pPrint $ quarter & dot
  PP.pPrint $ quarter & dot & toRatio
  PP.pPrint $ quarter & dot & dot
  PP.pPrint $ quarter & dot & dot & toRatio
  let exampleDuration =
        CompositeDuration [quarter & dot, CompositeDuration [quarter]]
  PP.pPrint $ exampleDuration
  PP.pPrint $ toRatio $ exampleDuration
