module Melos.Score.Patterns where

import Data.List
import qualified Data.List.Split as SL
import Prelude

forever' :: a -> [[a]]
forever' x = repeat [x]

alternate :: [[[a]]] -> [[a]]
alternate = concat . transpose

flattenOnce :: [[[a]]] -> [[a]]
flattenOnce = fmap concat . transpose

repeatN :: Int -> [[[a]]] -> [[a]]
repeatN count xs = fmap concat $ SL.chunksOf count $ flattenOnce xs

rotate :: [t] -> [t]
rotate [] = []
rotate (x : xs) = xs ++ [x]

chunksOf' :: [Int] -> [a] -> [[a]]
chunksOf' ns xs =
  take (head ns) xs :
  chunksOf' (rotate ns) (drop (head ns) xs)

repeatN' :: [Int] -> [[[a]]] -> [[a]]
repeatN' counts xs = fmap concat $ chunksOf' counts $ flattenOnce xs

everyN :: [Int] -> [[[t]]] -> [[t]]
everyN ns xs =
  concat $
    zipWith
      (\n xs' -> (take (n - 1) $ repeat []) ++ [xs'])
      (cycle ns)
      (flattenOnce xs)

once :: [[[t]]] -> [[t]]
once xs = concat [take 1 . flattenOnce $ xs, [[]]]
