{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell #-}

module Melos.Score.Tempo where

import Control.Monad.State
import qualified Data.IntervalMap.Generic.Strict as IM
import qualified Data.Map as Map
import Data.Ratio
import Lens.Micro.Platform
import Melos.Score.Duration
import Melos.Score.Interval
import Melos.Score.Span
import Text.Pretty.Simple (pPrint)

data Ramp a = Ramp
  { _rampLo :: a
  , _rampHi :: a
  }
  deriving (Eq, Show)

makeLenses ''Ramp

data Tempo a
  = Tempo a
  | TempoSlope a a
  deriving (Eq, Show)

makeLenses ''Tempo

average xs = sum xs / (length xs % 1)

tempoScale (tempo, tempoUnit) = 60 / ((tempo % 1) * (toRatio tempoUnit))

scaledDuration (duration, start, end) = duration * ((start + end) / 2)

scaledSpanDuration tempoSpans span@(start, end) =
  span
    & IM.intersecting tempoSpans
    & IM.toList
    & fmap go
    & sum
  where
    go ((x1, x2), Tempo x) = tempoScale x * (end' - start)
      where
        start' = max start x1
        end' = min end x2
    go ((x1, x2), TempoSlope y1 y2) =
      scaledDuration
        ( end' - start'
        , interpolate x1 x2 y1' y2' start'
        , interpolate x1 x2 y1' y2' end'
        )
      where
        y1' = tempoScale y1
        y2' = tempoScale y2
        start' = max start x1
        end' = min end x2

interpolate x1 x2 y1 y2 x = ((y2 - y1) * x + x2 * y1 - x1 * y2) / (x2 - x1)

onsetMap spans now xs = go now xs & Map.fromList
  where
    spans' = IM.fromList spans
    go now ((start, end) : xs) =
      -- TODO: avoid duplicates
      [ (start, now)
      , (end, now + duration)
      ]
        <> go (now + duration) xs
      where
        duration = scaledSpanDuration spans' (start, end)
    go _ _ = []

example =
  [1 .. 8]
    & fmap (\s -> ((s - 1) % 4, s % 4))
    & onsetMap tempoSpans 0
  where
    -- & fmap (fmap realToFrac)

    tempoSpans =
      [ ((0, 8), TempoSlope (60, quarter) (60, quarter))
      ]

main :: IO ()
main = do
  pPrint example

scaleSpanDur (Span start end _) (Span _ _ (Tempo tempo)) =
  tempoScale tempo * (end - start)
scaleSpanDur (Span start end content) (Span tempoStart tempoEnd@tempo (TempoSlope tempo1 tempo2)) =
  duration * ((startTempo + endTempo) / 2)
  where
    ts1 = tempoScale tempo1
    ts2 = tempoScale tempo2
    startTempo = interpolate tempoStart tempoEnd ts1 ts2 start
    endTempo = interpolate tempoStart tempoEnd ts1 ts2 end
    duration = end - start

spanStart (Span x _ _) = x

spanEnd (Span _ x _) = x

scaleSpans tempoL contentL initialSpan spans = result & closeSpanState' end
  where
    (end, result) = execState (traverse go spans) (0, SpanState 0 initialSpan [])
    go (Span start end params) = do
      let tempo = view tempoL params
          content = view contentL params
      when (start == spanStart content) $ do
        now' <- use _1
        _2 %= updateSpanState' now' (Just $ spanValue content)
      let duration = scaleSpanDur (Span start end ()) tempo
      _1 %= (+ duration)
