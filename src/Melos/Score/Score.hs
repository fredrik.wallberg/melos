{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell #-}

module Melos.Score.Score where

import Barbies
import Control.Monad.State
import Data.Ratio
import Lens.Micro.Platform
import Melos.Score.Duration
import Melos.Score.Prolation
import Melos.Score.Span (Span (..), SpanStateResult (..), updateSpanState)
import Melos.Score.Tree

data ProgramState a = ProgramState
  { _now :: Ratio Int
  , _prolations :: [Prolation]
  , _paramState :: a
  }
  deriving (Show, Functor)

makeLenses ''ProgramState

getProlation f dur xs = prolation
  where
    childDurations = fmap f xs
    childDurSum = sum childDurations
    prolation = childDurSum / dur

isEmptySpanStateResult (SpanStateResult []) = True
isEmptySpanStateResult _ = False

interpretScore (Tree dur xs) = do
  now' <- use now
  let prolation = getProlation childDuration (toRatio dur) xs & Prolation
  prolations %= (prolation :)
  xs' <- mapM interpretScore xs
  prolations %= drop 1
  return $ Tree ((now', now' + toRatio dur), dur, prolation) xs'
  where
    childDuration (Tree dur _) = toRatio dur
    childDuration (Leaf (Node dur _)) = toRatio dur
interpretScore (Leaf (Node dur x)) = do
  now' <- use now
  paramState %= (\s -> applyParams now' s x)
  currProlations <- use prolations
  let scaledDuration = toRatio dur / (product currProlations & prolationValue)
  now %= (+ scaledDuration)
  return $ Leaf (now', now' + scaledDuration, dur)
  where
    applyParams now a b =
      bzipWith (updateSpanState now) a b

segmentParams now s =
  if isEmpty s
    then []
    else
      Span now nextOnset' (bmap firstSpan s) :
      segmentParams nextOnset' (bmap (nextState nextOnset') s)
  where
    nextOnset' = nextOnset s
    firstSpan (SpanStateResult (x : _)) = x
    isEmpty s =
      s
        & bfoldMap (\x -> [isEmptySpanStateResult x])
        & and
    nextOnset s =
      s
        & bfoldMap (\(SpanStateResult (Span start end _ : _)) -> [end])
        & minimum
    nextState nextOnset xs'@(SpanStateResult (Span start end _ : xs)) =
      if end == nextOnset
        then SpanStateResult xs
        else xs'

t ::
  Duration NoteValue ->
  State [Tree (Duration NoteValue) (Node (f Maybe))] () ->
  State [Tree (Duration NoteValue) (Node (f Maybe))] ()
t duration body = do
  let children = execState body []
  let tree = Tree duration children
  modify (\xs -> xs <> [tree])

l ::
  Duration NoteValue ->
  f Maybe ->
  State [Tree (Duration NoteValue) (Node (f Maybe))] ()
l duration params = do
  let leaf = Leaf (Node duration $ params)
  modify (\xs -> xs <> [leaf])
