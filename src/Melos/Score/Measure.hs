{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell #-}

module Melos.Score.Measure where

import qualified Data.IntervalMap.Generic.Strict as IM
import Data.Ratio
import Lens.Micro.Platform
import Melos.Score.Duration
import Melos.Score.Interval
import Text.Pretty.Simple (pPrint)

data Empty

data Open

data Closed

data TimeSignature
  = Change
  | NoChange
  deriving (Show)

data MeasureSegment a = MeasureSegment
  { _onset :: Ratio Int
  , _duration :: Duration NoteValue
  , _tree :: a
  }
  deriving (Functor, Show, Foldable, Traversable)

instance Applicative MeasureSegment where
  pure x = MeasureSegment 0 whole x
  MeasureSegment a b f <*> MeasureSegment c d x = MeasureSegment c d (f x)

makeLenses ''MeasureSegment

newtype Span a b = Span b
  deriving (Show)

data Spans measure tempo = Spans
  { _measure :: measure
  , _tempo :: tempo
  }
  deriving (Functor, Show)

makeLenses ''Spans

changeSpan value now (Span (before, curr), acc) =
  (Span (now, value), ((before, now), curr) : acc)

closeSpan now (Span (before, curr), acc) =
  (Span (before, curr), ((before, now), curr) : acc)

setMeasure (now, (Spans measures tempi), results) =
  (now, Spans measures' tempi, results)
  where
    measures' = changeSpan () now measures

setTempo value s@(now, _, _) = s & _2 . tempo %~ changeSpan value now

end s@(now, Spans measures tempi, results) =
  s & _2 . measure %~ close & _2 . tempo %~ close & _3 %~ reverse
  where
    close span = span & closeSpan now & snd & reverse

segment duration tree (now, spans, result) =
  (now + toRatio duration, spans, s : result)
  where
    s = MeasureSegment now duration (fmap (\f -> f now duration) tree)

-- applySpans (Spans f g) (Spans x y) = Spans (f x) (g y)

-- joinSpans xs = foldl go (Spans [] []) xs
--   where
--     go (Spans x0 y0) (Spans x1 y1) = (Spans (x0 <> x1) (y0 <> y1))

-- getSpans spans ms = fmap go ms
--   where
--     getSpan onset duration spans =
--       IM.intersecting (IM.fromList spans) (onset, onset + toRatio duration)
--         & IM.toList
--         -- TODO: handle missing value?
--         & head
--     go (MeasureSegment onset duration _) =
--       MeasureSegment
--         onset
--         duration
--         ( applySpans
--             (Spans (getSpan onset duration) (getSpan onset duration))
--             spans
--         )

durationToTimeSignature denom duration = (duration * denom, denom)

pairwise f xs = go f Nothing xs
  where
    go f prev [] = []
    go f prev (x : xs) = f prev x : go f (Just x) xs

annotateMeasure Nothing (span@(start, end), b) = (span, Change)
annotateMeasure (Just ((start, end), _)) (span@(start', end'), b) =
  if end - start == end' - start' then (span, NoChange) else (span, Change)

mkGrid start tempo f = (duration, spans', segments)
  where
    spans' = spans & measure %~ pairwise annotateMeasure
    result@(duration, spans, segments) =
      ( start :: Ratio Int
      , Spans (Span (start :: Ratio Int, ()), []) (Span (start, tempo), [])
      , []
      )
        & f
        & end

-- -- skip duration = segment duration (\_ span -> span)
-- -- TODO: ensure tempo ramp always has at least two segments
-- -- TODO: ensure structure is closed ("end")
-- -- TODO: add first measure of kind annotation
-- -- TODO: rename skip -> ?
-- example =
--   ( segments
--       -- TODO: measures can be "annotated" for consecutive duplicates
--       -- before passing in to getSpans. Time signature can be resolved at
--       -- the same time.
--       -- & getSpans (spans & measure %~ pairwise annotateMeasure)
--       & getSpans spans'
--       & reverse
--   , spans'
--   )
--   where
--     s x = (0, Spans (Span (0, ()), []) (Span (0, x), []), [])
--     skip _ span = span
--     spans' = spans & measure %~ pairwise annotateMeasure
--     result@(duration, spans, segments) =
--       s "a"
--         & segment whole (\_ span -> span)
--         & segment whole (\_ span -> span)
--         & setMeasure
--         & segment whole (\_ span -> span)
--         & segment whole (\_ span -> span)
--         & setTempo "b"
--         & end

-- main :: IO ()
-- main = pPrint example
