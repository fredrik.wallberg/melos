{-# LANGUAGE TemplateHaskell #-}

module Melos.Score.Pitch where

import Lens.Micro.Platform

data PitchName
  = Ceses
  | Ces
  | C
  | Cis
  | Cisis
  | Deses
  | Des
  | D
  | Dis
  | Disis
  | Eses
  | Es
  | E
  | Eis
  | Eisis
  | Feses
  | Fes
  | F
  | Fis
  | Fisis
  | Geses
  | Ges
  | G
  | Gis
  | Gisis
  | Ases
  | As
  | A
  | Ais
  | Aisis
  | Beses
  | Bes
  | B
  | Bis
  | Bisis
  deriving (Bounded, Enum, Ord, Eq, Show)

toMidi'PitchName pc = case pc of
  Ceses -> -2
  Ces -> -1
  C -> 0
  Cis -> 1
  Cisis -> 2
  Deses -> 0
  Des -> 1
  D -> 2
  Dis -> 3
  Disis -> 4
  Eses -> 2
  Es -> 3
  E -> 4
  Eis -> 5
  Eisis -> 6
  Feses -> 3
  Fes -> 4
  F -> 5
  Fis -> 6
  Fisis -> 7
  Geses -> 5
  Ges -> 6
  G -> 7
  Gis -> 8
  Gisis -> 9
  Ases -> 7
  As -> 8
  A -> 9
  Ais -> 10
  Aisis -> 11
  Beses -> 9
  Bes -> 10
  B -> 11
  Bis -> 12
  Bisis -> 13

data Pitch12TET = Pitch12TET
  { _pitchName :: PitchName
  , _octave :: Int
  }
  deriving (Ord, Eq, Show)

makeLenses ''Pitch12TET

toMidi pitchName octave = toMidi'PitchName pitchName + octave * 12

pitchToMidi (Pitch12TET pitchName octave) = toMidi'PitchName pitchName + octave * 12

toFreq (Pitch12TET pitchName octave) = midiToHz $ toMidi pitchName octave

hzToMidi :: Floating a => a -> a
hzToMidi hz = 57.0 + 12.0 * logBase 2 (hz / 440.0)

midiToHz midi = 440 * (2 ** ((midi' - 69) / 12)) where midi' = realToFrac midi
