{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Melos.Score.Prolation where

import Data.Ratio

newtype Prolation = Prolation (Ratio Int)
  deriving newtype (Eq, Num, Show)

-- TODO: use named field instead
prolationValue (Prolation x) = x
