{-# LANGUAGE TemplateHaskell #-}

module Melos.Score.Logic where

import Control.Monad
import Control.Monad.Logic
import Control.Monad.State
import qualified Data.List as List
import Lens.Micro.Platform

data L a
  = One a
  | OneOf [L a]
  | AnyOrder [L a]
  | Every [L a]
  deriving (Show)

choices :: MonadPlus m => [a] -> m a
choices = msum . map return

oneOf actions = do
  s <- get
  action <- choices actions
  put s
  action

anyOrder xs = do
  s <- get
  xs' <- choices $ List.permutations xs
  put s
  sequence_ xs'

rotate = drop <> take

forward (x : xs) = xs <> [rotate 1 x]

skip (x : xs) = xs <> [x]

focus ((x : _) : _) = x

data Result = Success | Recoverable
  deriving (Eq)

data SearchState a = SearchState
  { _count :: Int
  , _maxCount :: Int
  , _end :: a -> Bool
  }

makeLenses ''SearchState

exceedsMaxCount (SearchState count maxCount _) = count > maxCount

ended (SearchState _ _ end) = end

circular' s sources = do
  when (exceedsMaxCount s) mzero
  programState <- get
  let action = focus sources
  result <- msplit action
  case result of
    Nothing -> do
      put programState
      circular' (s & count %~ (+ 1)) (skip sources)
    Just _ -> do
      programState <- get
      if ended s programState
        then return programState
        else circular' (s & count %~ (+ 1)) (forward sources)

runCircular ::
  Monad m =>
  SearchState b ->
  [[LogicT (StateT b m) a]] ->
  b ->
  m b
runCircular searchState sources programState = do
  result <-
    evalStateT
      (observeManyT 1 (circular' searchState sources))
      programState
  case result of
    [] -> error "No results"
    (x : _) -> return x

main = do
  let sources =
        [
          [ do
              modify (1 :)
              modify (2 :)
          , oneOf [mzero, sequence_ [modify (3 :), oneOf [mzero]]]
          ]
        , [modify (333 :)]
        ]
  let searchState =
        SearchState
          { _count = 0
          , _maxCount = 10000
          , _end = \xs -> length xs > 10
          }
  s0 <- runCircular searchState sources []
  mapM_ print (reverse s0)
