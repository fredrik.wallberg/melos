module Melos.Score.Dissonance where

import qualified Data.List as List
import qualified Data.Set as Set

data Interval
  = Unison
  | MinorSecond
  | MajorSecond
  | MinorThird
  | MajorThird
  | PerfectFourth
  | AugmentedFourth
  | PerfectFifth
  | MinorSixth
  | MajorSixth
  | MinorSeventh
  | MajorSeventh
  deriving (Bounded, Enum, Ord, Eq, Show)

interval12TETDissonance :: Num p => Interval -> p
interval12TETDissonance interval = case interval of
  Unison -> 0
  MinorSecond -> 13
  MajorSecond -> 5
  MinorThird -> 3
  MajorThird -> 2
  PerfectFourth -> 1
  AugmentedFourth -> 6
  PerfectFifth -> 1
  MinorSixth -> 2
  MajorSixth -> 3
  MinorSeventh -> 5
  MajorSeventh -> 13

interval12TETDissonance' :: Int -> Double
interval12TETDissonance' interval = case interval of
  0 -> 0
  1 -> 13
  2 -> 5
  3 -> 3
  4 -> 2
  5 -> 1
  6 -> 8
  7 -> 1
  8 -> 2
  9 -> 3
  10 -> 5
  11 -> 13

uniqueIntervals :: Ord b => [b] -> [(b, b)]
uniqueIntervals l = (List.nub [(x, y) | x <- l, y <- l, x < y])

dissonanceScore'12TET :: (Interval -> Int) -> [Int] -> Double
dissonanceScore'12TET _ pitches
  | Set.size pitchClasses' <= 1 = 0.0
  | otherwise = score' + (realToFrac unisonPenalty * 10) + crossingPenalty
  where
    pitches' = Set.fromList pitches
    pitchClasses' = Set.map (\x -> x `mod` 12) pitches'
    -- pitchClasses' = pitches'
    intervals' =
      map (\s -> interval12TETDissonance' (Set.findMax s - Set.findMin s)) $
        Set.toList $
          Set.filter (\x -> Set.size x == 2) (Set.powerSet pitchClasses')
    sum' = foldl (+) 0 intervals'
    score' = sum' / (realToFrac $ length intervals')
    unisonPenalty = (length pitches - Set.size pitches')
    crossingPenalty = if List.sort pitches == pitches then 0 else 100

interval12TETDissonance2 :: Int -> Int
interval12TETDissonance2 interval = case interval of
  0 -> 0
  1 -> 13
  2 -> 5
  3 -> 3
  4 -> 2
  5 -> 1
  6 -> 8
  7 -> 1
  8 -> 2
  9 -> 3
  10 -> 5
  11 -> 13

pairs :: [a] -> [(a, a)]
pairs l = [(x, y) | (x : ys) <- List.tails l, y <- ys]

dissonanceScore'12TET2 pitches
  | length pitches /= (length $ List.nub pitches) = 1000
  | List.sort pitches /= pitches = 1000
  | length pitches <= 1 = 0
  | otherwise = score'
  where
    allPairs = pairs pitches
    intervals = fmap (\(a, b) -> b - a) allPairs
    intervalScores =
      fmap (interval12TETDissonance2 . (\x -> x `mod` 12)) intervals
    sum' = foldl (+) 0 intervalScores
    score' = realToFrac sum' / (realToFrac $ length $ intervals)
