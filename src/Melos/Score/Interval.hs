{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Melos.Score.Interval where

import qualified Data.IntervalMap.Generic.Strict as IM

instance Ord e => IM.Interval (e, e) e where
  lowerBound (a, _) = a
  upperBound (_, b) = b
  rightClosed _ = False
