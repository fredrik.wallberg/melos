{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE TemplateHaskell #-}

module Melos.Score.Moment where

import Data.Ratio
import Lens.Micro.Platform
import Melos.Score.Duration (Duration (..), NoteValue (..))
import qualified Melos.Score.Duration as Duration
import Melos.Score.TimeSignature (TimeSignature (..))

data Moment a = Moment
  { _duration :: TimeSignature (Duration NoteValue)
  , _prolations :: [Ratio Int]
  , _path :: [Either (Ratio Int) (Ratio Int)]
  , _prolatedSpan :: (Ratio Int, Ratio Int)
  , _content :: a
  }
  deriving (Eq, Show, Functor, Foldable)

makeLenses ''Moment

getDuration' (Moment (TimeSignature duration) prolations _ _ _) =
  Duration.toRatio duration * (product prolations)

durationInSeconds :: Integral a => a -> Ratio a -> Ratio a -> Ratio a
durationInSeconds tempo tempoUnit duration =
  duration * (60 / ((tempo % 1) * tempoUnit))

start (Moment _ _ _ (x, _) _) = x

end (Moment _ _ _ (_, x) _) = x
