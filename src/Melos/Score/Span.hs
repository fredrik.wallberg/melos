{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE TemplateHaskell #-}

module Melos.Score.Span where

import Barbies
import Data.Ratio
import Lens.Micro.Platform

data SpanType a
  = HeadSpan a
  | MidSpan a
  | TailSpan a
  | FullSpan a
  deriving (Eq, Ord, Show, Functor, Foldable)

makeLenses ''SpanType

data SpanState a = SpanState (Ratio Int) a [Span a]
  deriving (Show, Functor)

spanStateValue (SpanState _ x _) = x

newtype SpanStateResult a = SpanStateResult [Span a]
  deriving (Show, Functor)

-- Spans use ratios, not musical durations
data Span a = Span (Ratio Int) (Ratio Int) a
  deriving (Eq, Show, Functor, Traversable, Foldable)

instance Applicative Span where
  pure x = Span 0 0 x
  (Span _ _ f) <*> (Span a b x) = Span a b (f x)

spanTypeValue s = case s of
  HeadSpan x -> x
  MidSpan x -> x
  TailSpan x -> x
  FullSpan x -> x

spanValue (Span _ _ x) = x

joinSpans (HeadSpan l) (MidSpan r) = Just $ HeadSpan l
joinSpans (HeadSpan l) (TailSpan r) = Just $ FullSpan l
joinSpans (MidSpan l) (MidSpan r) = Just $ MidSpan l
joinSpans (MidSpan l) (TailSpan r) = Just $ TailSpan r
joinSpans (FullSpan Nothing) (FullSpan Nothing) = Just $ FullSpan Nothing
joinSpans (FullSpan l) (FullSpan r) = Nothing
joinSpans _ _ = Nothing

updateSpanState now (SpanState before oldValue result) (Just newValue) =
  if now > before
    then SpanState now newValue (result <> [Span before now oldValue])
    else SpanState now newValue result
updateSpanState _ s Nothing = s

closeSpanState now (SpanState before oldValue result) =
  SpanStateResult $ result <> [Span before now oldValue]

updateSpanState' now Nothing s = s
updateSpanState' now (Just newValue) s@(SpanState before oldValue result) =
  if now > before
    then SpanState now newValue (result <> [Span before now oldValue])
    else SpanState now newValue result

closeSpanState' :: Ratio Int -> SpanState a -> [Span a]
closeSpanState' now (SpanState before value result) =
  if now > before
    then result <> [Span before now value]
    else result

resolveSpans (Span start end (duration, params)) =
  Span
    start
    end
    ( duration
    , params & bmap (go start end)
    )
  where
    go start end (Span start' end' x)
      | start == start' && end == end' = FullSpan x
      | start == start' = HeadSpan x
      | end == end' = TailSpan x
      | start > start' = MidSpan x
      | otherwise = error "Invalid span"
