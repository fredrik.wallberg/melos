{-# LANGUAGE TemplateHaskell #-}

module Melos.Score.TimeSignature where

import Lens.Micro.Platform

data TimeSignature a = TimeSignature
  { _getTimeSignature :: a
  }
  deriving (Eq, Ord, Show)

makeLenses ''TimeSignature
