{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Melos.Score.Stream where

newtype Stream a = Stream [a]
  deriving (Functor, Semigroup, Monoid)

mkStream xs = Stream (cycle xs)

dropFromStream (Stream (x : xs)) = Stream xs

takeFromStream (Stream (x : xs)) = x
