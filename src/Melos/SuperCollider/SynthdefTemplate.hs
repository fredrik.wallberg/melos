{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}
module Melos.SuperCollider.SynthdefTemplate where

import Data.ByteString.Char8 as Char8
import Data.Char
import Data.Data
import Language.Haskell.TH
import qualified Sound.SC3.Server.Graphdef as R

lowercase :: [Char] -> [Char]
lowercase [] = []
lowercase (x : xs) = toLower x : xs

titleCase :: [Char] -> [Char]
titleCase [] = []
titleCase (x : xs) = toUpper x : xs

defBang :: Bang
defBang = Bang NoSourceUnpackedness NoSourceStrictness

stringType :: Type
stringType = ConT ''String

mkRecord2 :: Monad m => String -> [Char] -> [((ByteString, b1), b2)] -> m [Dec]
mkRecord2 filePath name cls = do
  return $
    [ DataD [] typeName [PlainTV (mkName "a")] Nothing [constr] derivClause'
    , ValD (VarP syndefFileName) (NormalB (LitE (StringL filePath))) []
    ]
  where
    syndefFileName = mkName $ lowercase name ++ "SyndefFile"
    derivClause' =
      [ DerivClause
          Nothing
          (fmap ConT [''Show, ''Functor, ''Typeable, ''Data, ''Foldable])
      ]
    typeName = mkName . titleCase $ name
    constr = RecC typeName fields
    fields = fmap mkField cls
    mkField ((fieldName, _), _) =
      ( mkName $ ((lowercase name) ++ (titleCase (Char8.unpack fieldName)))
      , defBang
      , VarT (mkName "a")
      )

mkArgRecord :: FilePath -> Q [Dec]
mkArgRecord filePath = do
  (name, params) <- runIO $ do
    gr <- R.read_graphdef_file filePath
    return (R.graphdef_name gr, R.graphdef_controls gr)
  mkRecord2 filePath (Char8.unpack name) params
