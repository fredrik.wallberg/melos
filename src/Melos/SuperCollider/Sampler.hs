module Melos.SuperCollider.Sampler where

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Ord as Ord
import Debug.Trace

data Sample = Sample
  { sampleFilePath :: FilePath
  , sampleBaseFrequency :: Double
  , samplePlaybackRate :: Double
  }
  deriving (Ord, Eq, Show)

getByFreq :: Double -> Map.Map Double FilePath -> Sample
getByFreq k m =
  trace
    (show k)
    ( Sample
        { sampleFilePath = snd elem'
        , sampleBaseFrequency = fst elem'
        , samplePlaybackRate = k / (fst elem')
        }
    )
  where
    elem' = case (Map.lookupLE k m, Map.lookupGE k m) of
      (Just below, Just above) ->
        List.minimumBy (Ord.comparing (byDistance k)) [below, above]
      (Just below, Nothing) -> below
      (Nothing, Just above) -> above
      (Nothing, Nothing) -> error "no match"
    byDistance k' (a, _) = abs (k' - a)
