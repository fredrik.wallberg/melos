{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}

module Melos.SuperCollider.NRT where

import Control.Monad.State
import Data.Char
import Data.Data
import Data.Foldable
import qualified Data.List as List
import qualified Data.Map as Map
import Data.Ord
import qualified Data.Set as Set
import Debug.Trace
import Sound.OSC
import Sound.SC3
import qualified Sound.SC3.Server.Graphdef as R
import System.FilePath

data NRTState = NRTState
  { activeBuffers :: Map.Map String Int
  , activeUGens :: Set.Set FilePath
  , bufcount :: Int
  , synthId :: Int
  }
  deriving (Eq, Show)

lowercase :: [Char] -> [Char]
lowercase [] = []
lowercase (x : xs) = toLower x : xs

loadBuffer :: Buffer_Id -> String -> Bundle
loadBuffer bufnum path = bundle 0 [b_allocRead bufnum path 0 0]

synthAt :: Time -> Double -> Synth_Id -> Bundle
synthAt x f id_ = bundle x [s_new "asdf" id_ AddToTail 0 [("freq", f)]]

playSample :: Time -> Double -> Synth_Id -> Bundle
playSample x f id_ = bundle x [s_new "asdf" id_ AddToTail 0 [("freq", f)]]

formatFieldName :: [Char] -> [Char]
formatFieldName = dropWhile isLower

argNames :: Data a => a -> [[Char]]
argNames d = fmap (lowercase . formatFieldName) . constrFields . toConstr $ d

argValues :: Foldable t => t a -> [a]
argValues d = Data.Foldable.toList d

args :: (Data (t b), Foldable t) => t b -> [([Char], b)]
args d = zipWith (,) (argNames d) (argValues d)

toSynthname :: Typeable a => a -> [Char]
toSynthname t = lowercase . takeWhile (\x -> x /= ' ') . show . typeOf $ t

loadSynths :: FilePath -> NRTState -> IO [Bundle]
loadSynths syndefDir (NRTState _ ugens _ _) = mapM loadSynth (Set.toList ugens)
  where
    getPath name = syndefDir </> (name ++ ".scsyndef")
    loadSynth name = do
      graph <- R.read_graphdef_file (getPath name)
      return $ bundle 0 [d_recv_gr graph]

loadBuffers :: NRTState -> [Bundle]
loadBuffers (NRTState buffers _ _ _) =
  trace (show buffers) $
    fmap loadBuffer' (Map.toList buffers)
  where
    loadBuffer' (path, bufnum) = bundle 0 [b_allocRead bufnum path 0 0]

initialState :: NRTState
initialState = NRTState Map.empty Set.empty 0 0

scEventsToBundles' syndefDir f gestures = do
  let buffers = loadBuffers state'
  synths <- loadSynths syndefDir state'
  let bundles = buffers <> synths <> concat events
  return $ bundles
  where
    (events, state') = result
    result :: ([[Bundle]], NRTState)
    result = runState (mapM f gestures) (NRTState Map.empty Set.empty 0 0)

runSupercollider' syndefDir outFile gestureToBundles gestures = do
  bundles <- scEventsToBundles' syndefDir gestureToBundles gestures
  let score = NRT $ List.sortBy (comparing bundleTime) bundles
  writeNRT outFile $
    nrt_append
      score
      (NRT $ [bundle (nrt_end_time score + 20) [c_set [(0, 0)]]])
