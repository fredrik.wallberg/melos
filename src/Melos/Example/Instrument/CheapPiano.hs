{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell #-}

module Melos.Example.Instrument.CheapPiano where

import qualified Control.Monad.State as State
import qualified Data.Aeson as Aeson
import qualified Data.ByteString.Lazy as BL
import Data.Function
import qualified Data.Map as Map
import qualified Data.Maybe as Maybe
import qualified Data.Set as Set
import Lens.Micro.Platform
import qualified Melos.Score.Pitch as Pitch
import qualified Melos.Score.Span as Span
import qualified Melos.Score.Tempo as Tempo
import qualified Melos.SuperCollider.NRT as NRT
import qualified Melos.SuperCollider.Sampler as Sampler
import qualified Melos.SuperCollider.SynthdefTemplate as SynthdefTemplate
import qualified Sound.OSC as OSC
import qualified Sound.SC3 as SC3

SynthdefTemplate.mkArgRecord "resources/synthdefs/cheappiano.scsyndef"

synth :: Cheappiano Double
synth =
  Cheappiano
    { cheappianoOut = 0
    , cheappianoFreq = 440
    , cheappianoAmp = 0.1
    , cheappianoDur = 1
    , cheappianoPan = 0
    }

paramToScBundles ::
  Map.Map Double FilePath ->
  (Double, Double, Int) ->
  NRT.NRTState ->
  ([OSC.Bundle], NRT.NRTState)
paramToScBundles sampleMap (start, stop, pitch) s@(NRT.NRTState buffers ugens bufcount synthId) =
  (bundles, NRT.NRTState buffers activeUGens' bufcount synthId')
  where
    duration = stop - start
    freq = Pitch.midiToHz pitch
    activeUGens' = Set.insert (NRT.toSynthname synth) ugens
    synthId' = synthId + 1
    synthBundle synthId =
      OSC.bundle
        start
        [ SC3.s_new
            (NRT.toSynthname synth)
            synthId'
            SC3.AddToHead
            0
            ( NRT.args $
                synth
                  { cheappianoFreq = freq
                  , cheappianoDur = duration
                  }
            )
        ]
    bundles = [synthBundle synthId']

scSpanToOSCBundle spans =
  State.state $ paramToScBundles Map.empty spans
