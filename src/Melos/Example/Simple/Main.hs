module Melos.Example.Simple.Main where

import Control.Monad.State
import Lens.Micro.Platform
import Melos.Score.Duration (
  dot,
  eighth,
  half,
  quarter,
  sixteenth,
  toRatio,
  whole,
 )
import qualified Melos.Score.Duration as Duration
import Melos.Score.Pitch
import Melos.Score.Score
import Melos.Score.Tempo
import Text.Pretty.Simple (pPrint)
import Melos.Example.Simple.Setup

score = do
  replicateM_ 2 $ do
    forM_ [half & dot, half] $ \measureDuration -> do
      t measureDuration $ do
        t half $ do
          l quarter $
            emptyParams & voice1 ?~ Pitch (Pitch12TET E 5)
          l quarter $
            emptyParams & voice2 ?~ Pitch (Pitch12TET D 5)
        l quarter $ emptyParams
          & voice1 ?~ Rest
          & voice3 ?~ Pitch (Pitch12TET C 5)

main = do
  (voices, spans) <- runScore score
  -- Render score as pdf via lilypond
  renderLilypond voices
  -- Render a soundfile using non-real-time synthesis in SuperCollider
  renderPiano spans
