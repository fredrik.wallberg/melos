{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}

module Melos.Example.Simple.Setup where

import Barbies
import Control.Monad.State
import Data.Maybe
import Data.Ratio
import Data.Text.Prettyprint.Doc
import qualified GHC.Generics as Generics
import Lens.Micro.Platform
import qualified Melos.Lilypond.Printer as Printer
import Melos.Score.Duration (
  eighth,
  half,
  quarter,
  sixteenth,
  toRatio,
  whole,
 )
import qualified Melos.Score.Duration as Duration
import Melos.Score.Pitch
import Melos.Score.Score
import Melos.Score.Span
import Melos.Score.Tempo
import Melos.Score.Tree
import Melos.Score.Utils
import Melos.Example.Instrument.CheapPiano as Piano
import qualified Melos.SuperCollider.NRT as NRT
import qualified Data.Foldable as Foldable
import System.Directory
import System.IO
import System.Process
import System.Timeout
import Data.Text.Prettyprint.Doc.Render.Text

import Text.Pretty.Simple (pPrint)

data Event = Rest | Pitch Pitch12TET
  deriving (Eq, Show)

makeLenses ''Event

data Voices a = Voices
  { _v1 :: a
  , _v2 :: a
  , _v3 :: a
  }
  deriving (Eq, Show, Foldable, Functor, Traversable)

makeLenses ''Voices

instance Applicative Voices where
  pure x = Voices x x x
  Voices f1 f2 f3 <*> Voices x1 x2 x3 = Voices (f1 x1) (f2 x2) (f3 x3)

data ScoreParams f = ScoreParams
  { _tempo :: f (Tempo (Int, Duration.NoteValue))
  , _voice1 :: f Event
  , _voice2 :: f Event
  , _voice3 :: f Event
  }
  deriving
    ( Generics.Generic
    , FunctorB
    , TraversableB
    , ApplicativeB
    , ConstraintsB
    )

deriving instance AllBF Show f ScoreParams => Show (ScoreParams f)

deriving instance AllBF Eq f ScoreParams => Eq (ScoreParams f)

makeLenses ''ScoreParams

emptyParams :: ScoreParams Maybe
emptyParams = bpure Nothing

data VoiceParams f = VoiceParams
  { _voiceTempo :: f (Tempo (Int, Duration.NoteValue))
  , _voice :: f Event
  }
  deriving
    ( Generics.Generic
    , FunctorB
    , TraversableB
    , ApplicativeB
    , ConstraintsB
    )

deriving instance AllBF Show f VoiceParams => Show (VoiceParams f)

deriving instance AllBF Eq f VoiceParams => Eq (VoiceParams f)

makeLenses ''VoiceParams

runScore score = do
  let score' = execState score []
      (tree, spanState) = runState (mapM interpretScore score') initialState
      end = spanState & view now
      spans =
        spanState & view paramState
          & bmap (closeSpanState end)
          & segmentParams 0
      tempo' = spanState ^. paramState . tempo
      voices =
        (\voice -> VoiceParams tempo' (spanState ^. paramState . voice))
          <$> Voices voice1 voice2 voice3
          & fmap (segmentParams 0 . bmap (closeSpanState end))
  return
    (
      voices & fmap (evalState (mapM insertSpan tree))
    , voices
    )
  where
    initialState =
      ProgramState
        0
        []
        ( ScoreParams
            { _tempo = f (TempoSlope (120, Duration.Quarter) (60, Duration.Quarter))
            , _voice1 = f Rest
            , _voice2 = f Rest
            , _voice3 = f Rest
            }
        )
      where
        f :: forall a. a -> SpanState a
        f x = SpanState 0 x []

instance Printer.LilypondMusic (Duration.Duration Duration.NoteValue, VoiceParams SpanType) where
  toMusic (Duration.SimpleDuration duration, VoiceParams tempo event) =
    Printer.Music $
      catMaybes
        [ tempo
            & spanToTempo
            & fmap formatTempo
        , Just $ note (catMaybes [tie])
        ]
    where
      formatTempo (Tempo (a, b)) = Printer.Tempo a b
      formatTempo (TempoSlope (a, b) (c, d)) = Printer.Tempo a b
      note = case spanTypeValue event of
        Rest -> Printer.Rest duration
        (Pitch pitch) -> Printer.Note pitch duration
      tie = case event of
        HeadSpan _ -> Just Printer.Tie
        MidSpan _ -> Just Printer.Tie
        _ -> Nothing
      spanToTempo (HeadSpan x) = Just x
      spanToTempo _ = Nothing

makePdf workingDirectory book = do
  dir <- canonicalizePath workingDirectory
  withCurrentDirectory dir $ do
    let filename = "lilytest.ly"
    withFile filename WriteMode (\h -> hPutDoc h book)
    exitCode <- system "lilypond lilytest.ly"
    print exitCode

renderLilypond voices = makePdf "/tmp" score
  where
    resolvedSpans = voices & fmap (fmap (fmap resolveSpans))
    staff voice =
      Printer.Staff
        []
        Printer.TrebleClef
        [ Printer.Voice
            ""
            []
            (fmap Printer.measureToLilypond (view voice resolvedSpans))
        ]
    score = pretty $ Printer.Score [ Printer.StaffGroup "group 1" [] (fmap staff [v1, v2, v3])]

renderPiano spans = do
  let
    syndefDir = "files/supercollider"
    scsynth = "/Applications/SuperCollider.app/Contents/Resources/scsynth"
    oscFile = "/tmp/t.osc"
    audioFile = "/tmp/out.aiff"
    sampleRate = 44100
    numChannels = 2
    cmd = unwords
      [
        scsynth
      , "-N"
      , oscFile
      , "_"
      , audioFile
      , show sampleRate
      , "AIFF"
      , "int16"
      , "-o"
      , show numChannels
      ]
  NRT.runSupercollider' syndefDir oscFile Piano.scSpanToOSCBundle scSpans
  exitCode <- system cmd
  print exitCode
  where
    scSpans = spans
        & fmap (scaleSpans voiceTempo voice Rest)
        & fmap (mapMaybe toSc)
        & Foldable.toList
        & concat
    toSc (Span start end (Pitch (Pitch12TET pitchName octave))) =
      Just (realToFrac start, realToFrac end, toMidi pitchName octave)
    toSc (Span _ _ Rest) = Nothing
