{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

module Melos.Lilypond.Printer where

import Control.Monad.Reader
import qualified Data.Char
import qualified Data.Either as Either
import qualified Data.IntervalMap.Generic.Strict as IM
import qualified Data.Maybe as Maybe
import Data.Ratio
import Data.Text.Prettyprint.Doc
import Lens.Micro.Platform
import qualified Melos.Score.Duration as Duration
import qualified Melos.Score.Measure as Measure
import Melos.Score.Moment (Moment (..))
import Melos.Score.Pitch (Pitch12TET (..))
-- import Melos.Score.TimeSignature (TimeSignature (..))

import Melos.Score.Prolation
import Melos.Score.Span
import qualified Melos.Score.Tempo as Tempo
import Melos.Score.Tree

class LilypondMusic a where
  toMusic :: a -> Music

newtype Book a = Book
  { bookParts :: [BookPart a]
  }
  deriving (Ord, Eq, Show, Functor, Foldable)

data BookPart a = BookPart
  { headerBlock :: HeaderBlock
  , score :: a
  }
  deriving (Ord, Eq, Show, Functor, Foldable)

newtype HeaderBlock = HeaderBlock
  { headerBlock_Content :: [String]
  }
  deriving (Ord, Eq, Show)

newtype PaperBlock = PaperBlock
  { paperBlock_Content :: [String]
  }
  deriving (Ord, Eq, Show)

newtype LayoutBlock = LayoutBlock
  { layoutBlock_Content :: [String]
  }
  deriving (Ord, Eq, Show)

newtype Score a = Score
  { staffGroups :: [a]
  }
  deriving (Ord, Eq, Show, Functor, Foldable)

data StaffGroup a = StaffGroup
  { staffGroup_Name :: String
  , staffGroup_Overrides :: [Override]
  , staves :: [a]
  }
  deriving (Ord, Eq, Show, Functor, Foldable)

data Staff a = Staff
  { staff_Overrides :: [Override]
  , staff_DefaultClef :: Clef
  , voices :: [a]
  }
  deriving (Ord, Eq, Show, Functor, Foldable)

data Voice a = Voice
  { name :: String
  , voice_Overrides :: [Override]
  , music :: a
  }
  deriving (Ord, Eq, Show, Functor, Foldable)

data DefaultRender
  = AsRest
  | AsSkip
  | HideMultiMeasureRest
  deriving (Ord, Eq, Show)

data Clef
  = BassClef
  | AltoClef
  | TrebleClef
  deriving (Ord, Eq, Show)

data Music
  = Note
      { _notePitch :: Pitch12TET
      , _noteDuration :: Duration.NoteValue
      , _noteSpans :: [LilypondSpan]
      }
  | Chord
      { _chordPitches :: [Pitch12TET]
      , _chordDuration :: Duration.NoteValue
      , _chordSpans :: [LilypondSpan]
      }
  | Skip
      { _skipDuration :: Duration.NoteValue
      , _skipSpans :: [LilypondSpan]
      }
  | Rest
      { _restDuration :: Duration.NoteValue
      , _restSpans :: [LilypondSpan]
      }
  | Music {_music :: [Music]}
  | Tempo
      { _tempoBpm :: Int
      , _tempoUnit :: Duration.NoteValue
      }
  | Clef Clef
  | TimeSignature {_timeSignature :: Duration.Duration Duration.NoteValue}
  | Tuplet
      { _tupletProlation :: Prolation
      , _tupletMusic :: [Music]
      }
  deriving (Eq, Show)

data LilypondSpan
  = Tie
  | StartCresc
  | EndCresc
  deriving (Eq, Show)

newtype Override = Override String
  deriving (Eq, Ord, Show)

measureToLilypond (Tree (Span _ _ (duration, prolation)) xs) =
  [TimeSignature duration]
    <> fmap treeToLilypond xs
    & container
  where
    container = if prolation == 1 then Music else Tuplet prolation

treeToLilypond (Tree (Span _ _ (duration, prolation)) xs) =
  fmap treeToLilypond xs
    & container
  where
    container = if prolation == 1 then Music else Tuplet prolation
treeToLilypond (Leaf span@(Span _ _ (duration, params))) =
  toMusic (duration, params)

---------------------------------------------------------------------
-- Pretty printer
---------------------------------------------------------------------

withBraces :: Doc ann -> Doc ann
withBraces x = vcat [lbrace, x, rbrace]

withBracesList :: [Doc ann] -> Doc ann
withBracesList x = vcat [lbrace, indent 2 (vcat x), rbrace]

withAngles :: Doc ann -> Doc ann
withAngles x =
  langle <> langle <> line <> indent 2 x <> line <> rangle <> rangle

instance Pretty PaperBlock where
  pretty (PaperBlock paperContent) =
    pretty "\\paper" <+> withBracesList (fmap pretty paperContent)

instance Pretty LayoutBlock where
  pretty (LayoutBlock layoutContent) =
    pretty "\\layout" <+> withBracesList (fmap pretty layoutContent)

instance Pretty a => Pretty (Book a) where
  pretty (Book xs) =
    pretty "\\book" <+> withBracesList (fmap pretty xs)

instance Pretty HeaderBlock where
  pretty (HeaderBlock header) =
    pretty "\\header" <+> withBracesList (fmap pretty header)

instance Pretty a => Pretty (BookPart a) where
  pretty (BookPart header score) =
    pretty "\\bookpart"
      <+> withBracesList [pretty header, pretty score]

instance Pretty a => Pretty (Score a) where
  pretty (Score xs) =
    pretty "\\score"
      <+> withBracesList (fmap pretty xs)

instance Pretty a => Pretty (StaffGroup a) where
  pretty (StaffGroup name overrides staves) =
    vcat
      [ pretty "\\new StaffGroup"
      , formatName instrumentName
      , withAngles $ vcat $ fmap pretty staves
      ]
    where
      formatName name = pretty "\\with" <+> name
      instrumentName =
        withBraces $
          indent 2 $
            pretty "instrumentName"
              <+> equals
              <+> pretty "#"
              <> dquote
              <> pretty name
              <> dquote

instance Pretty (Voice [Music]) where
  pretty (Voice voiceName _ music) =
    pretty "\\new Voice"
      <+> withBracesList
        [ pretty voiceName
        , pretty "%----------VOICE" <+> pretty voiceName
        , vcat (fmap pretty music)
        ]

instance Pretty a => Pretty (Staff a) where
  pretty (Staff overrides clef voices) =
    vcat
      [ pretty "\\new Staff"
      , withAngles $
          vcat
            [ pretty clef
            , vcat $ fmap pretty overrides
            , vcat $ fmap pretty voices
            ]
      ]

instance Pretty Music where
  pretty (Note pitch duration spans) =
    hcat
      [ pretty pitch
      , pretty duration
      , hcat (fmap pretty spans)
      ]
  pretty (Music body) = vcat $ fmap pretty body
  pretty (Tuplet prolation body) =
    vcat
      [ pretty prolation
      , withBracesList $ fmap pretty body
      ]
  pretty (TimeSignature duration) = case duration of
    Duration.NonReducedDuration (times, dur) ->
      pretty "\\time"
        <+> pretty times
        <> slash
        <> pretty dur
    Duration.SimpleDuration dur ->
      pretty "\\time"
        <+> (dur' & numerator & pretty)
        <> slash
        <> (dur' & denominator & pretty)
      where
        dur' = Duration.toRatio dur
  pretty (Rest duration _) =
    hcat
      [ pretty "r"
      , pretty duration
      ]
  pretty (Tempo bpm tempoUnit) =
    pretty "\\tempo"
      <+> pretty tempoUnit
      <+> equals
      <+> pretty bpm

instance Pretty Override where
  pretty (Override s) = pretty s

instance Pretty Pitch12TET where
  pretty (Pitch12TET pitchName octave) =
    pretty pitchName' <> octaveTick
    where
      pitchName' = Data.Char.toLower <$> show pitchName
      octaveTick = toLilyOctave octave

instance Pretty LilypondSpan where
  pretty Tie = pretty "~"

instance Pretty Clef where
  pretty clef = case clef of
    BassClef -> pretty "\\clef bass"
    AltoClef -> pretty "\\clef alto"
    TrebleClef -> pretty "\\clef treble"

instance Pretty Prolation where
  pretty (Prolation prolation) =
    pretty "\\override TupletNumber.text = #tuplet-number::calc-fraction-text"
      <+> pretty "\\tuplet"
      <+> formatRatio prolation

instance Pretty Duration.NoteValue where
  pretty Duration.Whole = pretty "1"
  pretty Duration.Half = pretty "2"
  pretty Duration.Quarter = pretty "4"
  pretty Duration.Eighth = pretty "8"
  pretty Duration.Sixteenth = pretty "16"
  pretty Duration.ThirtySecond = pretty "32"
  pretty Duration.SixtyFourth = pretty "64"
  pretty (Duration.Dotted dur) = g 1 dur
    where
      g depth (Duration.Dotted dur) = g (depth + 1) dur
      g depth dur = pretty dur <> pretty (concat (replicate depth "."))

formatBeam tempoUnit path
  | null path' = emptyDoc
  | all Either.isLeft path' = pretty "["
  | all Either.isRight path' = pretty "]"
  | otherwise = emptyDoc
  where
    pathValue (Left x) = x
    pathValue (Right x) = x
    path' = takeWhile (\x -> pathValue x <= tempoUnit) path

toLilyOctave :: (Eq a, Num a, Show a) => a -> Doc ann
toLilyOctave x = case x of
  1 -> pretty ",,,"
  2 -> pretty ",,"
  3 -> pretty ","
  4 -> emptyDoc
  5 -> pretty "'"
  6 -> pretty "''"
  7 -> pretty "'''"
  8 -> pretty "''''"
  _ -> error (show x)

formatRatio r =
  pretty (numerator r)
    <> slash
    <> pretty (denominator r)
